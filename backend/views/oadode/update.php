<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Oadode */
/* @var $modelsDescriptions */

$this->title = 'Update Oadode: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oadodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="oadode-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsDescriptions' => $modelsDescriptions
    ]) ?>

</div>
