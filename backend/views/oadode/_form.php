<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Oadode */
/* @var $form yii\widgets\ActiveForm */
/* @var $modelsDescriptions */
?>

<div class="oadode-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'legal_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_mailing_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_email')->textInput(['maxlength' => true]) ?>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Description of Goods</h4></div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsDescriptions[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'application_id',
                    'customer_id',
                    'user_id',
                    'description',
                    'ecl_group',
                    'ecl_item',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php /** @var $modelDescrription backend\models\DescriptionOfGoods */ ?>
                <?php foreach ($modelsDescriptions as $i => $modelDescription): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Description of goods</h3>
                            <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (! $modelDescription->isNewRecord) {
                                echo Html::activeHiddenInput($modelDescription, "[{$i}]id");
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($modelDescription, "[{$i}]description")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelDescription, "[{$i}]ecl_group")->textInput() ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelDescription, "[{$i}]ecl_item")->textInput() ?>
                                </div>
                            </div><!-- .row -->
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <?= $form->field($model, 'application_type')->radioList([
            'New',
            'Re-Assessment'
    ]) ?>

    <?= $form->field($model, 'business_title')->radioList([
        'Owner',
        'Authorized Individual',
        'Designated Official',
        'Officer',
        'Director',
        'Employee'
    ]) ?>

    <?= $form->field($model, 'lang')->radioList([
        'English',
        'French'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
