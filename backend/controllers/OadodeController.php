<?php

namespace backend\controllers;

use backend\models\DescriptionOfGoods;
use Yii;
use backend\models\Oadode;
use backend\models\OadodeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OadodeController implements the CRUD actions for Oadode model.
 */
class OadodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Oadode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OadodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Oadode model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Oadode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Oadode();
        $modelsDescriptions = [new DescriptionOfGoods];

        if ($model->load(Yii::$app->request->post())) {

            $modelsToValidate = [];
            $allValid = true;
            $mainModelValid = false;

            if(isset($_POST['DescriptionOfGoods'])) {
                foreach ($_POST['DescriptionOfGoods'] as $descriptionOfGood) {
                    $descriptionModel = new DescriptionOfGoods();
                    $descriptionModel->description = $descriptionOfGood['description'];
                    $descriptionModel->ecl_group = $descriptionOfGood['ecl_group'];
                    $descriptionModel->ecl_item = $descriptionOfGood['ecl_item'];
                    // no user is logged in as of now. I would usually make a relation between the Oadode and Description table to make my life a bit easier
                    // for now I will just make all the user_ids 1
                    $descriptionModel->user_id = 1;

                    $modelsToValidate[] = $descriptionModel;

                }

                foreach ($modelsToValidate as $toValidate) {
                    if(!$toValidate->validate()) {
                        $allValid = false;
                    }
                }

                if($model->validate()) {
                    $mainModelValid = true;
                }

                if($mainModelValid && $allValid) {
                    $model->save(false);
                    foreach ($modelsToValidate as $modelToSave) {
                        $modelToSave->save(false);
                    }
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'modelsDescriptions' => (empty($modelsDescriptions)) ? [new DescriptionOfGoods] : $modelsDescriptions
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelsDescriptions' => (empty($modelsDescriptions)) ? [new DescriptionOfGoods] : $modelsDescriptions
        ]);
    }

    /**
     * Updates an existing Oadode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsDescriptions = DescriptionOfGoods::findAll(['user_id' => 1]);


        if ($model->load(Yii::$app->request->post())) {
            $modelsToValidate = [];
            $allValid = true;
            $mainModelValid = false;

            if(isset($_POST['DescriptionOfGoods'])) {
                foreach ($_POST['DescriptionOfGoods'] as $descriptionOfGood) {
                    $descriptionModel = new DescriptionOfGoods();
                    $descriptionModel->id = $descriptionOfGood['id'];
                    $descriptionModel->description = $descriptionOfGood['description'];
                    $descriptionModel->ecl_group = $descriptionOfGood['ecl_group'];
                    $descriptionModel->ecl_item = $descriptionOfGood['ecl_item'];
                    // no user is logged in as of now. I would usually make a relation between the Oadode and Description table to make my life a bit easier
                    // for now I will just make all the user_ids 1
                    $descriptionModel->user_id = 1;

                    $modelsToValidate[] = $descriptionModel;

                }

                foreach ($modelsToValidate as $toValidate) {
                    if(!$toValidate->validate()) {
                        $allValid = false;
                    }
                }

                if($model->validate()) {
                    $mainModelValid = true;
                }

                if($mainModelValid && $allValid) {
                    // needs try catch
                    $model->delete();
                    $model->save(false);

                    foreach ($modelsDescriptions as $modelToDelete) {
                        $modelToDelete->delete();
                    }

                    foreach ($modelsToValidate as $modelToSave) {
                        $modelToSave->id = null;
                        $modelToSave->save(false);
                    }
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'modelsDescriptions' => (empty($modelsDescriptions)) ? [new DescriptionOfGoods] : $modelsDescriptions
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelsDescriptions' => (empty($modelsDescriptions)) ? [new DescriptionOfGoods] : $modelsDescriptions
        ]);
    }

    /**
     * Deletes an existing Oadode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Oadode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Oadode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Oadode::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
