<?php

use yii\db\Migration;

/**
 * Class m210406_190729_add_int_to_oadode
 */
class m210406_190729_add_int_to_oadode extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%oadode}}', 'lang', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%oadode}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210406_190729_add_int_to_oadode cannot be reverted.\n";

        return false;
    }
    */
}
